from clease import Concentration, CECrystal, CEBulk, NewStructures, Evaluate
from clease.tools import reconfigure
from ase.visualize import view
from ase.db import connect
from clease import PhysicalRidge,LinearRegression
from clease.basis_function import BinaryLinear



con = Concentration(basis_elements=(['Mg', 'Sn'],['X','Mg'], ['Sn']))

setting = CECrystal(cellpar=[6.75, 6.75, 6.75, 90, 90, 90],
                     basis=[(0.25, 0.25, 0.25), (0.75,0.75,0.75), (0, 0, 0)],
                     concentration=con,
                     spacegroup=216,
                     size=[(-2, 2, 2), (2, -2, 2), (1, 1, -1)],
                     db_name='MgSn_216.db',
                     supercell_factor=20,
                     max_cluster_size=4,
                     max_cluster_dia=[8.0, 8.0, 7.0])

#setting.view_clusters()
setting.basis_func_type="binary_linear"
reconfigure(setting)

#select_cond=[('converged','=',True),('gen','<',3)]

evl = Evaluate(setting)


evl.set_fitting_scheme(fitting_scheme='l1', alpha=1e-9)
alpha = evl.plot_CV(num_alpha=100)
evl.set_fitting_scheme(fitting_scheme='l1', alpha=alpha)
evl.save_eci(fname='eci_MgSn_216')
evl.plot_fit(interactive=True) 
evl.plot_ECI()