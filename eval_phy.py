from clease import Concentration, CECrystal, CEBulk, NewStructures, Evaluate
from clease.tools import reconfigure
from ase.visualize import view
from ase.db import connect
from clease import PhysicalRidge,LinearRegression
from clease.physical_ridge import random_cv_hyper_opt
import numpy as np

con = Concentration(basis_elements=(['Mg', 'Sn'],['X','Mg'], ['Sn']))

setting = CECrystal(cellpar=[6.75, 6.75, 6.75, 90, 90, 90],
                     basis=[(0.25, 0.25, 0.25), (0.75,0.75,0.75), (0, 0, 0)],
                     concentration=con,
                     spacegroup=216,
                     size=[(-2, 2, 2), (2, -2, 2), (1, 1, -1)],
                     db_name='MgSn_216.db',
                     supercell_factor=20,
                     max_cluster_size=4,
                     max_cluster_dia=[8.0, 8.0, 7.0])

#setting.view_clusters()
setting.basis_func_type="binary_linear"
reconfigure(setting)
evl = Evaluate(setting)
X = evl.cf_matrix
y = evl.e_dft
names = evl.cf_names

reg = PhysicalRidge()
reg.sizes_from_names(names)
reg.diameters_from_names(names)
params = {
        'lamb_size': np.logspace(-12, 4, 10000).tolist(),
        'lamb_dia': np.logspace(-12, 4, 10000).tolist(),
        'dia_decay': ['linear', 'exponential'],
        'size_decay': ['linear', 'exponential']
}
res = random_cv_hyper_opt(reg, params, X, y, cv=10, num_trials=1000)
print(res)
